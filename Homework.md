# Homework

## Part 1. File upload test
### Site under test: https://imgur.com

### Test case 1. File Upload Protection -- Extension validation

| Test step                                                  | Result                                                    | 
| ---------------------------------------------------------- | --------------------------------------------------------  |
| Open imgur.com                                             | OK                                                        |
| Click "New post"                                           | Upload media dialog opens                                 |
| Drag file with .png.js extension onto the dotted zone      | Error notification appears. File not uploaded             |
| Drag file with extension .js%00.png onto the dotted zone   | File is uploaded but rejected by site. No visible effect  |
| Click "New post"                                           | Upload media dialog opens                                 |
| Click Choose photo/Video                                   | Dolphin file picker opens. All non-image files are hidden. File with .png.js is visible. |
| Select .png.js file. Click Open                            | Error notification appears. File not uploaded             |
| Select .png file. Click Open                               | File is uploaded successfully. Post created               |

Comment: Imgur image extension validation works fine. However, the Dolphin file
picker has a bug that allows to select files with ".png" substring anywhere in
the filename when only files with png extension are expected.

### Test case 2. File Upload Protection -- Upload and Download Limits

| Test step                                                  | Result                                                    | 
| ---------------------------------------------------------- | --------------------------------------------------------- |
| Pick a valid png image.                                    | OK                                                        |
| Resize the png file size with `truncate -s 100M pic.png`   | OK                                                        |
| Open imgur.com                                             | OK                                                        |
| Click "New post"                                           | Upload media dialog opens                                 |
| Drag png file with onto the dotted zone                    | Error notification appears (Size too large). File not uploaded |

Comment: Imgur size validation works fine. 

## Part 2. Login test
### Site under test: https://www.cyberforum.ru/

### Test case 3. Forgot password -- Return a consistent message for both existent and non-existent accounts.
### Test case 4. Forgot password -- Ensure that the time taken for the user response message is uniform.

| Test step                                                  | Result                                                    | 
| ---------------------------------------------------------- | --------------------------------------------------------- |
| Open cyberforum.ru                                         | OK                                                        |
| Click "Восстановить пароль"                                | Password restore page opened                              |
| Enter email that has an account on the forum.              | OK                                                        |
| Fill CAPTCHA.                                              | OK                                                        |
| Press "Восстановить имя пользователя / пароль"             | After a long delay message "Ваше имя пользователя и информация о том, как сменить пароль, высланы вам по электронной почте. Теперь вы будете перемещены туда, где были." shows. |
| Open cyberforum.ru                                         | OK                                                        |
| Click "Восстановить пароль"                                | Password restore page opened                              |
| Enter email that has no account on the forum.              | OK                                                        |
| Fill CAPTCHA.                                              | OK                                                        |
| Press "Восстановить имя пользователя / пароль"             | After a short delay message "Вы указали неизвестный нам адрес электронной почты. Попробуйте другой или свяжитесь с администрацией." appears.

Comment: The forum treats security of its users very poor. Neither the message
for the existent and nonexistent account is the same nor the time for 
processing of those requests.

